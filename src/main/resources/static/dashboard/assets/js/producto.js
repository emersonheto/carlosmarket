$('body').on('click','.editarProducto',function(){
	$('#id').val($(this).attr('id'));
	$('#nombre').val($(this).attr('param1'));
	$('#cantidad').val($(this).attr('param2'));
	$('#precio').val($(this).attr('param3'));
	$('#descripcion').val($(this).attr('param4'));
	document.getElementById("categoria.id").value=$(this).attr('param5');
	document.getElementById("proveedor.id").value=$(this).attr('param6');
	$('#titleModal').html("Editar Producto");
	$('#productosModal').modal({show:true});
});

$("#btnNueva").click(function(){
	$('#titleModal').html("Nuevo Producto");
	$('#id').val('');
	$('#nombre').val('');
	$('#cantidad').val('');
	$('#precio').val('');
	$('#descripcion').val('');
});