$('body').on('click','.editarCategoria',function(){
	$('#id').val($(this).attr('id'));
	$('#nombre').val($(this).attr('param1'));
	$('#descripcion').val($(this).attr('param2'));
	$('#titleModal').html("Editar Categoría");
	$('#categoriaModal').modal({show:true});
});

$("#btnNueva").click(function(){
	$('#titleModal').html("Nueva Categoría");
	$('#id').val('');
	$('#nombre').val('');
	$('#descripcion').val('');
});