$(document).ready(function() {
	$('#datatable').DataTable({
		"pagingType": "full_numbers",
		"lengthMenu": [
			[5, 10, 15, -1],
			[5, 10, 15, "All"]
		],
		responsive: true,
		language: {
			search: "_INPUT_",
			searchPlaceholder: "Search records",
		}
	});
});