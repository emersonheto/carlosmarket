package com.cmarket.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cmarket.model.Cliente;
@Repository
public interface IClienteDAO extends CrudRepository<Cliente, Long>{

}
