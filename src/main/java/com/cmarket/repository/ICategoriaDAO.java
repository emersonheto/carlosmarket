package com.cmarket.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cmarket.model.Categoria;
@Repository
public interface ICategoriaDAO extends CrudRepository<Categoria, Long> {

}
