package com.cmarket.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cmarket.model.DetalleBoleta;

@Repository
public interface IDetalleBoletaDAO extends CrudRepository<DetalleBoleta, Long> {

}
