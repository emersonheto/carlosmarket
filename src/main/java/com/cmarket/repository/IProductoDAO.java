package com.cmarket.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cmarket.model.Producto;

@Repository
public interface IProductoDAO extends CrudRepository<Producto, Long> {

	@Query("select p from Producto p where p.categoria.id=?1")
	List<Producto> findByCategoria(Long id);
}
