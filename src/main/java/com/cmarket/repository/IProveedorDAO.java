package com.cmarket.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cmarket.model.Proveedor;

@Repository
public interface IProveedorDAO extends CrudRepository<Proveedor, Long> {

}
