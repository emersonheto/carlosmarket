package com.cmarket.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cmarket.model.Usuario;
@Repository
public interface IUsuarioDAO extends CrudRepository<Usuario, Long>{

	Optional<Usuario> findByEmail(String email);
	@Query("select u from Usuario u where u.tipo=?1")
	List<Usuario> findByTipo(String tipo);
	@Query("select count(u) from Usuario u where u.tipo=?1")
	long countByTipo(String tipo);
	
}
