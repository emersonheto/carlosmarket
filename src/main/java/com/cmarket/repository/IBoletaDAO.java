package com.cmarket.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cmarket.model.Boleta;
import com.cmarket.model.Usuario;
@Repository
public interface IBoletaDAO extends CrudRepository<Boleta, Long> {

	List<Boleta> findByUsuario(Usuario usuario);
	@Query("select count(b) from Boleta b where b.estado=?1")
	long countByEstadi(Boolean estado);
	
	@Query("select count(b) from Boleta b where b.tipo=?1")
	long countByTipo(String estado);
	
	@Query("select count(b) from Boleta b where b.entrega=?1 and b.pago=?2")
	long countFinalizadas(Boolean estado,Boolean pago);
	
	@Query("select b from Boleta b where b.estado=?1 and b.entrega=?2")
	List<Boleta> findByEstado(Boolean estado,Boolean entrega);
	
	@Query("select b from Boleta b where b.tipo=?1")
	List<Boleta> findByTipo(String estado);
	
	
	@Modifying
	@Transactional
	@Query("update Boleta b set b.estado=true where b.id=?1")
	void updateEstado(Long id);
	
	@Modifying
	@Transactional
	@Query("update Boleta b set b.pago=true where b.id=?1")
	void updatePago(Long id);
	
	@Modifying
	@Transactional
	@Query("update Boleta b set b.entrega=true where b.id=?1")
	void updateEntrega(Long id);
	
}
