package com.cmarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarlosMarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarlosMarketApplication.class, args);
	}

}
