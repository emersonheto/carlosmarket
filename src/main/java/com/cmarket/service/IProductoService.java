package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import com.cmarket.model.Producto;



public interface IProductoService {

	public List<Producto>listar();
	public Producto save(Producto producto);
	public Optional<Producto> get(Long id);
	public void update(Producto producto);
	public void delete(Long id);
	public long contar();
	public List<Producto> findByCategoria(Long id);
	
}
