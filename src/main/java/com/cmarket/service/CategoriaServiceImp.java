package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmarket.model.Categoria;
import com.cmarket.repository.ICategoriaDAO;
@Service
public class CategoriaServiceImp implements ICategoriaService{

	@Autowired
	private ICategoriaDAO categoriaDAO;
	
	@Override
	public List<Categoria> listar() {
		return (List<Categoria>) categoriaDAO.findAll();
	}

	@Override
	public Categoria save(Categoria categoria) {
		return categoriaDAO.save(categoria);
	}

	@Override
	public Optional<Categoria> get(Long id) {
		
		return categoriaDAO.findById(id);
	}

	@Override
	public void update(Categoria categoria) {
		categoriaDAO.save(categoria);
		
	}

	@Override
	public void delete(Long id) {
		categoriaDAO.deleteById(id);
		
	}

	@Override
	public long contar() {
		return categoriaDAO.count();
	}

}
