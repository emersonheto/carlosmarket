package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import com.cmarket.model.Usuario;

public interface IUsuarioService {
	
	public List<Usuario>listar();
	public Usuario save(Usuario usuario);
	public Optional<Usuario> get(Long id);
	public void update(Usuario usuario);
	public void delete(Long id);
	Optional<Usuario> findByEmail(String email);
	public List<Usuario> findByTipo(String tipo);
	public long countByTipo(String tipo);
}
