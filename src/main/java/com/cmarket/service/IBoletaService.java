package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import com.cmarket.model.Boleta;
import com.cmarket.model.Usuario;

public interface IBoletaService {
	
	
	List<Boleta> fiandAll();
	Boleta save(Boleta boleta);
	String numBoleta();
	List<Boleta> findByUsuario(Usuario usuario);
	Optional<Boleta> findById(Long id);
	public long contar();
	long countByEstadi(Boolean estado);
	List<Boleta> findByEstado(Boolean estado,Boolean entrega);
	void updateEstado(Long id);
	void updatePago(Long id);
	List<Boleta> findByTipo(String estado);
	void updateEntrega(Long id);
	long countFinalizadas(Boolean estado,Boolean pago);
	long countByTipo(String estado);
}
