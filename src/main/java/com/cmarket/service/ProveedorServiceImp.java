package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmarket.model.Proveedor;
import com.cmarket.repository.IProveedorDAO;

@Service
public class ProveedorServiceImp implements IProveedorService {

	@Autowired
	private IProveedorDAO proveedorDAO;

	@Override
	public List<Proveedor> listar() {
		return (List<Proveedor>) proveedorDAO.findAll();
	}

	@Override
	public Proveedor save(Proveedor proveedor) {
		return proveedorDAO.save(proveedor);
	}

	@Override
	public Optional<Proveedor> get(Long id) {
		return proveedorDAO.findById(id);
	}

	@Override
	public void update(Proveedor categoria) {
		proveedorDAO.save(categoria);

	}

	@Override
	public void delete(Long id) {
		proveedorDAO.deleteById(id);

	}

	@Override
	public long contar() {
		return proveedorDAO.count();
	}

}
