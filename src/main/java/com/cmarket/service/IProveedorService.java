package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import com.cmarket.model.Proveedor;

public interface IProveedorService {

	public List<Proveedor> listar();

	public Proveedor save(Proveedor proveedor);

	public Optional<Proveedor> get(Long id);

	public void update(Proveedor categoria);

	public void delete(Long id);

	public long contar();
}
