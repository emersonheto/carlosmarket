package com.cmarket.service;

import com.cmarket.model.DetalleBoleta;

public interface IDetalleBoletaService {

	DetalleBoleta save(DetalleBoleta detalleBoleta);
}
