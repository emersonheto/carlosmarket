package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmarket.model.Cliente;
import com.cmarket.repository.IClienteDAO;
@Service
public class ClienteServiceImp implements  IClienteService{
	
	@Autowired
	private IClienteDAO clienteDAO;

	@Override
	public List<Cliente> listar() {
		return (List<Cliente>) clienteDAO.findAll();
	}

	@Override
	public Cliente save(Cliente cliente) {
		return clienteDAO.save(cliente);
	}

	@Override
	public Optional<Cliente> get(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Cliente cliente) {
		clienteDAO.save(cliente);
		
	}

	@Override
	public void delete(Long id) {
		clienteDAO.deleteById(id);
		
	}

	@Override
	public long contar() {
		return clienteDAO.count();
	}

}
