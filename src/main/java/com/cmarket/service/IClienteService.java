package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import com.cmarket.model.Cliente;

public interface IClienteService {

	public List<Cliente>listar();
	public Cliente save(Cliente cliente);
	public Optional<Cliente> get(Long id);
	public void update(Cliente cliente);
	public void delete(Long id);
	public long contar();
	
}
