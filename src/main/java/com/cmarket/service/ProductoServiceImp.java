package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmarket.model.Producto;
import com.cmarket.repository.IProductoDAO;
@Service
public class ProductoServiceImp implements IProductoService {

	@Autowired
	private IProductoDAO productoDAO;
	
	@Override
	public Producto save(Producto producto) {
		return productoDAO.save(producto);
	}

	@Override
	public Optional<Producto> get(Long id) {
		return productoDAO.findById(id);
	}

	@Override
	public void update(Producto producto) {
		productoDAO.save(producto);
	}

	@Override
	public void delete(Long id) {
		productoDAO.deleteById(id);
		
	}

	@Override
	public List<Producto> listar() {
		return (List<Producto>)productoDAO.findAll();
	}

	@Override
	public long contar() {
		
		return productoDAO.count();
	}

	@Override
	public List<Producto> findByCategoria(Long id) {
		// TODO Auto-generated method stub
		return productoDAO.findByCategoria(id);
	}







}
