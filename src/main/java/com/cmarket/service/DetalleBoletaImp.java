package com.cmarket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmarket.model.DetalleBoleta;
import com.cmarket.repository.IDetalleBoletaDAO;

@Service

public class DetalleBoletaImp implements IDetalleBoletaService {

	@Autowired
	private IDetalleBoletaDAO detalleBoletaDAO;
	
	@Override
	public DetalleBoleta save(DetalleBoleta detalleBoleta) {
		return detalleBoletaDAO.save(detalleBoleta);
	}

}
