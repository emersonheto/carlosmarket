package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import com.cmarket.model.Categoria;


public interface ICategoriaService {
	
	public List<Categoria>listar();
	public Categoria save(Categoria categoria);
	public Optional<Categoria> get(Long id);
	public void update(Categoria categoria);
	public void delete(Long id);
	public long contar();
}
