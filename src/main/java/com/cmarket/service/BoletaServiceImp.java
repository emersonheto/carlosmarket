package com.cmarket.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmarket.model.Boleta;
import com.cmarket.model.Usuario;
import com.cmarket.repository.IBoletaDAO;

@Service
public class BoletaServiceImp implements IBoletaService {

	@Autowired
	private IBoletaDAO boletaDAO;
	
	
	@Override
	public Boleta save(Boleta boleta) {
		return boletaDAO.save(boleta);
	}

	@Override
	public List<Boleta> fiandAll() {
		return (List<Boleta>) boletaDAO.findAll();
	}
	
	@Override
	public String numBoleta() {
		long num=0;
		String numBoleta="";
		
		List<Boleta> boletas=fiandAll();
		List<Long> numerosBoleta=new ArrayList<Long>();
		
		boletas.stream().forEach(o -> numerosBoleta.add(Long.valueOf(o.getNumero())));
		
		if(boletas.isEmpty()) {
			num=1;
		}else {
			num=numerosBoleta.stream().max(Long::compare).get();
			num++;
		}
		
		if(num<10) {
			numBoleta="000000000"+String.valueOf(num);
		}else if(num<100){
			numBoleta="00000000"+String.valueOf(num);
		}else if(num<1000){
			numBoleta="0000000"+String.valueOf(num);
		}else if(num<10000){
			numBoleta="000000"+String.valueOf(num);
		}else if(num<100000){
			numBoleta="00000"+String.valueOf(num);
		}
		
		return numBoleta;
	}

	@Override
	public List<Boleta> findByUsuario(Usuario usuario) {
		return boletaDAO.findByUsuario(usuario);
	}

	@Override
	public Optional<Boleta> findById(Long id) {
		
		return boletaDAO.findById(id);
	}

	@Override
	public long contar() {
		return boletaDAO.count();
	}

	@Override
	public long countByEstadi(Boolean estado) {
		// TODO Auto-generated method stub
		return boletaDAO.countByEstadi(estado);
	}



	@Override
	public void updateEstado(Long id) {
		boletaDAO.updateEstado(id);
		
	}

	@Override
	public void updatePago(Long id) {
		boletaDAO.updatePago(id);		
	}

	@Override
	public List<Boleta> findByTipo(String estado) {
		return boletaDAO.findByTipo(estado);
	}

	@Override
	public void updateEntrega(Long id) {
		boletaDAO.updateEntrega(id);
		
	}

	@Override
	public List<Boleta> findByEstado(Boolean estado, Boolean entrega) {
		// TODO Auto-generated method stub
		return boletaDAO.findByEstado(estado, entrega);
	}

	@Override
	public long countFinalizadas(Boolean estado, Boolean pago) {
		// TODO Auto-generated method stub
		return boletaDAO.countFinalizadas(estado, pago);
	}

	@Override
	public long countByTipo(String estado) {
		// TODO Auto-generated method stub
		return boletaDAO.countByTipo(estado);
	}





	

	
	
}
