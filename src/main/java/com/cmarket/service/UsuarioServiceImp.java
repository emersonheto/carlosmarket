package com.cmarket.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmarket.model.Usuario;
import com.cmarket.repository.IUsuarioDAO;
@Service
public class UsuarioServiceImp implements IUsuarioService {

	@Autowired
	private IUsuarioDAO usuarioDAO;


	@Override
	public List<Usuario> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usuario save(Usuario usuario) {
		return usuarioDAO.save(usuario);
	}

	@Override
	public Optional<Usuario> get(Long id) {
		// TODO Auto-generated method stub
		return usuarioDAO.findById(id);
	}

	@Override
	public void update(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Optional<Usuario> findByEmail(String email) {
		return usuarioDAO.findByEmail(email);
	}

	@Override
	public List<Usuario> findByTipo(String tipo) {
		return usuarioDAO.findByTipo(tipo);
	}

	@Override
	public long countByTipo(String tipo) {
		return usuarioDAO.countByTipo(tipo);
	}


}
