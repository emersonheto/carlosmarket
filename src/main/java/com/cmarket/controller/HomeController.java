package com.cmarket.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cmarket.model.Boleta;
import com.cmarket.model.Categoria;
import com.cmarket.model.DetalleBoleta;
import com.cmarket.model.Producto;
import com.cmarket.model.Usuario;
import com.cmarket.repository.IDetalleBoletaDAO;
import com.cmarket.service.IBoletaService;
import com.cmarket.service.ICategoriaService;
import com.cmarket.service.IDetalleBoletaService;
import com.cmarket.service.IProductoService;
import com.cmarket.service.IUsuarioService;

@Controller
public class HomeController {

	@Autowired
	private IProductoService productoService;

	@Autowired
	private ICategoriaService categoriaService;

	@Autowired
	private IUsuarioService usuarioService;

	@Autowired
	private IBoletaService boletaService;

	@Autowired
	private IDetalleBoletaService detalleBoletaService;

	// para almacenar detalles de la boleta
	private List<DetalleBoleta> detalles = new ArrayList<DetalleBoleta>();
	// datos de la orden
	Boleta boleta = new Boleta();

	@RequestMapping("/")
	public String inicio(Model model, HttpSession session) {
		model.addAttribute("categorias", categoriaService.listar());
		model.addAttribute("productos", productoService.listar());
		//
		model.addAttribute("var", session.getAttribute("userid"));
		System.out.print("userid  " + session.getAttribute("userid"));

		return "user/home/index";
	}

	@GetMapping("/productohome/{id}")
	public String productoHome(@PathVariable Long id, Model model, HttpSession session) {
		model.addAttribute("var", session.getAttribute("userid"));
		model.addAttribute("categorias", categoriaService.listar());
		Producto producto = new Producto();
		Optional<Producto> productoOptional = productoService.get(id);
		producto = productoOptional.get();
		model.addAttribute("producto", producto);
		return "user/home/productohome";
	}

	@PostMapping("/carrito")
	public String carrito(@RequestParam Long id, @RequestParam Integer cantidad, Model model, HttpSession session) {
		model.addAttribute("var", session.getAttribute("userid"));
		model.addAttribute("categorias", categoriaService.listar());
		model.addAttribute("sesion", session.getAttribute("userid"));
		DetalleBoleta detalleBoleta = new DetalleBoleta();
		Producto producto = new Producto();

		double sumaTotal = 0;

		Optional<Producto> optionalProducto = productoService.get(id);

		producto = optionalProducto.get();

		detalleBoleta.setCantidad(cantidad);
		detalleBoleta.setPrecio(producto.getPrecio());
		detalleBoleta.setNombre(producto.getNombre());
		DecimalFormat df = new DecimalFormat("#.00");
		detalleBoleta.setTotal(Double.valueOf(df.format(producto.getPrecio() * cantidad)));
		detalleBoleta.setProducto(producto);

		// validar q el producto no se añada mas de 2 veces
		Long idProducto = producto.getId();
		boolean ingresado = detalles.stream().anyMatch(p -> p.getProducto().getId() == idProducto);
		if (!ingresado) {
			detalles.add(detalleBoleta);
		}

		sumaTotal = detalles.stream().mapToDouble(dt -> dt.getTotal()).sum();

		boleta.setTotal(sumaTotal);
		model.addAttribute("carrito", detalles);
		model.addAttribute("boleta", boleta);
		return "user/home/carrito";
	}

	// quitar producto del carrito
	@GetMapping("/carrito/eliminar/{id}")
	public String deleteItem(@PathVariable Long id, Model model) {
		model.addAttribute("categorias", categoriaService.listar());
		List<DetalleBoleta> nuevaOrden = new ArrayList<DetalleBoleta>();

		for (DetalleBoleta detalle : detalles) {
			if (detalle.getProducto().getId() != id) {
				nuevaOrden.add(detalle);
			}
		}
		detalles = nuevaOrden;

		double sumaTotal = 0;
		sumaTotal = detalles.stream().mapToDouble(dt -> dt.getTotal()).sum();

		boleta.setTotal(sumaTotal);
		model.addAttribute("carrito", detalles);
		model.addAttribute("boleta", boleta);

		return "user/home/carrito";
	}

	@GetMapping("/vercarrito")
	public String verCarrito(Model model, HttpSession session) {
		model.addAttribute("var", session.getAttribute("userid"));
		model.addAttribute("categorias", categoriaService.listar());
		model.addAttribute("carrito", detalles);
		model.addAttribute("boleta", boleta);
		return "user/home/carrito";
	}

	@GetMapping("/resumenorden")
	public String resumen(Model model, HttpSession session) {

		model.addAttribute("var", session.getAttribute("userid"));
		if (session.getAttribute("userid") == null) {
			return "redirect:/user/login";
		}
		model.addAttribute("categorias", categoriaService.listar());
		Usuario usuario = usuarioService.get(Long.parseLong(session.getAttribute("userid").toString())).get();
		model.addAttribute("usuario", usuario);
		model.addAttribute("carrito", detalles);
		model.addAttribute("boleta", boleta);
		return "user/home/resumenorden";
	}
    //en tienda contraentrega
	@GetMapping("/gbtc")
	public String guardarBoletatienda(HttpSession session) {
		Date fechaActual = new Date();
		boleta.setLlegada(fechaActual);
		boleta.setNumero(boletaService.numBoleta());
		Usuario usuario = usuarioService.get(Long.parseLong(session.getAttribute("userid").toString())).get();
		boleta.setUsuario(usuario);
		boleta.setEstado(false);
		boleta.setTipo("tienda");
		boleta.setEntrega(false);
		boleta.setPago(false);
		boletaService.save(boleta);

		for (DetalleBoleta db : detalles) {
			db.setBoleta(boleta);
			Optional<Producto> producto = productoService.get(db.getProducto().getId());
			// producto.get().setCantidad(producto.get().getCantidad()-db.getCantidad());
			producto.get().setCantidad(producto.get().getCantidad() - (int) db.getCantidad());
			productoService.save(producto.get());

			detalleBoletaService.save(db);
		}

		// vacear lista

		boleta = new Boleta();
		detalles.clear();

		return "redirect:/gracias";
	}

	//guardar boleta delivery contraenrega
	
	@GetMapping("/gbdc")
	public String guardarBoletdelivery(HttpSession session) {
		Date fechaActual = new Date();
		boleta.setLlegada(fechaActual);
		boleta.setNumero(boletaService.numBoleta());
		Usuario usuario = usuarioService.get(Long.parseLong(session.getAttribute("userid").toString())).get();
		boleta.setUsuario(usuario);
		boleta.setEstado(false);
		boleta.setTipo("delivery");
		boleta.setEntrega(false);
		boleta.setPago(false);
		boletaService.save(boleta);

		for (DetalleBoleta db : detalles) {
			db.setBoleta(boleta);
			Optional<Producto> producto = productoService.get(db.getProducto().getId());
			// producto.get().setCantidad(producto.get().getCantidad()-db.getCantidad());
			producto.get().setCantidad(producto.get().getCantidad() - (int) db.getCantidad());
			productoService.save(producto.get());

			detalleBoletaService.save(db);
		}

		// vacear lista

		boleta = new Boleta();
		detalles.clear();

		return "redirect:/gracias";
	}
	//en tienda con tarjeta
	@GetMapping("/gbtt")
	public String guardarBoletaDelivery(HttpSession session) {
		Date fechaActual = new Date();
		boleta.setLlegada(fechaActual);
		boleta.setNumero(boletaService.numBoleta());
		Usuario usuario = usuarioService.get(Long.parseLong(session.getAttribute("userid").toString())).get();
		boleta.setUsuario(usuario);
		boleta.setEstado(false);
		boleta.setTipo("tienda");
		boleta.setEntrega(false);
		boleta.setPago(true);
		boletaService.save(boleta);

		for (DetalleBoleta db : detalles) {
			db.setBoleta(boleta);
			Optional<Producto> producto = productoService.get(db.getProducto().getId());
			// producto.get().setCantidad(producto.get().getCantidad()-db.getCantidad());
			producto.get().setCantidad(producto.get().getCantidad() - (int) db.getCantidad());
			productoService.save(producto.get());

			detalleBoletaService.save(db);
		}

		// vacear lista

		boleta = new Boleta();
		detalles.clear();

		return "redirect:/gracias";
	}
	
	//delivery pagado
	@GetMapping("/gbdt")
	public String guardarBoletdeliveryP(HttpSession session) {
		Date fechaActual = new Date();
		boleta.setLlegada(fechaActual);
		boleta.setNumero(boletaService.numBoleta());
		Usuario usuario = usuarioService.get(Long.parseLong(session.getAttribute("userid").toString())).get();
		boleta.setUsuario(usuario);
		boleta.setEstado(false);
		boleta.setTipo("delivery");
		boleta.setEntrega(false);
		boleta.setPago(true);
		boletaService.save(boleta);

		for (DetalleBoleta db : detalles) {
			db.setBoleta(boleta);
			Optional<Producto> producto = productoService.get(db.getProducto().getId());
			// producto.get().setCantidad(producto.get().getCantidad()-db.getCantidad());
			producto.get().setCantidad(producto.get().getCantidad() - (int) db.getCantidad());
			productoService.save(producto.get());

			detalleBoletaService.save(db);
		}

		// vacear lista

		boleta = new Boleta();
		detalles.clear();

		return "redirect:/gracias";
	}
	

	private final org.slf4j.Logger logger = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping("/vercategoria/{id}")
	public String filtro(@PathVariable Long id, Model model) {
		model.addAttribute("categorias", categoriaService.listar());
		model.addAttribute("productos", productoService.findByCategoria(Long.valueOf(id)));
		return "user/home/index";
	}

	@RequestMapping("/buscar")
	public String buscarProducto(@RequestParam String nombre, Model model, HttpSession session) {
		model.addAttribute("categorias", categoriaService.listar());
		model.addAttribute("var", session.getAttribute("userid"));
		List<Producto> productos = productoService.listar().stream().filter(p -> p.getNombre().contains(nombre))
				.collect(Collectors.toList());
		model.addAttribute("productos", productos);
		return "user/home/index";
	}

	@RequestMapping("/gracias")
	public String gracias() {
		return "user/home/gracias";
	}
	
}
