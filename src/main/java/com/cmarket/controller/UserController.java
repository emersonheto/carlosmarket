package com.cmarket.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cmarket.model.Boleta;
import com.cmarket.model.Cliente;
import com.cmarket.model.Usuario;
import com.cmarket.service.IBoletaService;
import com.cmarket.service.ICategoriaService;
import com.cmarket.service.IClienteService;
import com.cmarket.service.IUsuarioService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IUsuarioService usuarioServce;
	
	@Autowired
	private IBoletaService boletaService;
	
	@Autowired
	private ICategoriaService categoriaService;
	
	@Autowired
	private IClienteService clienteService;
	
	@GetMapping("/registro")
	public String registo(Model model) {
		Usuario usuario=new Usuario();
		model.addAttribute("usuario",usuario);
		return "user/home/registro";
	}
	
	@RequestMapping("/listar")
	public String listar(Model model) {
		
		return "";
	}
	
	@PostMapping("/guardar")
	public String guardarUser(Usuario usuario) {
		usuario.setTipo("USER");
		usuarioServce.save(usuario);
		Cliente cliente=new Cliente();
		cliente.setUsuario(usuario);
		clienteService.save(cliente);
		return "redirect:/";
	}
	
	@RequestMapping("/login")
	public String login() {
		return "user/home/login";
	}
	private final org.slf4j.Logger logger=  LoggerFactory.getLogger(HomeController.class);
	@GetMapping("/acceder")
	public String logearse(Usuario usuario,HttpSession sesion) {
		
		Optional<Usuario> user=usuarioServce.findByEmail(usuario.getEmail());
		if(user.isPresent()) {
			sesion.setAttribute("userid", user.get().getId());
			if(user.get().getTipo().equals("ADMIN")) {
				return "redirect:/admin/vista";
			}else {
				return "redirect:/";
			}
			
		}
		
		return"redirect:/";
	}
	
	@RequestMapping("/compras")
	public String compras(Model model,HttpSession session) {
		model.addAttribute("var",session.getAttribute("userid"));
		model.addAttribute("categorias", categoriaService.listar());
		Usuario usuario=usuarioServce.get(Long.parseLong(session.getAttribute("userid").toString())).get();
		List<Boleta> boletas=boletaService.findByUsuario(usuario);
		model.addAttribute("boletas",boletas);
		return "user/home/compras";
	}
	
	@RequestMapping("/comprasdetalles/{id}")
	public String detalles(@PathVariable Long id,  Model model,HttpSession session) {
		Optional<Boleta> boleta=boletaService.findById(id);
		
		model.addAttribute("detalles",boleta.get().getDetalle());
		
		model.addAttribute("var",session.getAttribute("userid"));
		return "user/home/detallecompra";
	}
	
	
	@RequestMapping("/salir")
	public String cerrar(HttpSession session) {
		session.removeAttribute("userid");
		return "redirect:/";
	}
	
}
