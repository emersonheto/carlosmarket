package com.cmarket.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.cmarket.model.Boleta;
import com.cmarket.model.Categoria;
import com.cmarket.model.Cliente;
import com.cmarket.model.Producto;
import com.cmarket.model.Proveedor;
import com.cmarket.service.IBoletaService;
import com.cmarket.service.ICategoriaService;
import com.cmarket.service.IClienteService;
import com.cmarket.service.IProductoService;
import com.cmarket.service.IProveedorService;
import com.cmarket.service.UploadFileService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private IProductoService productoService;

	@Autowired
	private ICategoriaService categoriaService;

	@Autowired
	private IProveedorService proveedorService;

	@Autowired
	private IClienteService clienteService;
	
	@Autowired
	private IBoletaService boletaService;
	
	@Autowired
	private UploadFileService upload;

	@GetMapping("/vista")
	public String inicio(Model model) {
		List<Producto> productos = productoService.listar();
		model.addAttribute("productos", productos);
		return "admin/inicio/index";
	}

	
	@GetMapping("/dashboard")
	public String dashboard(Model model) {
		model.addAttribute("numproductos",productoService.contar());
		model.addAttribute("numcategorias",categoriaService.contar());
		model.addAttribute("numproveedores",proveedorService.contar());
		model.addAttribute("numclientes",clienteService.contar());
		model.addAttribute("numboletas",boletaService.contar());
		model.addAttribute("recibidas",boletaService.countByEstadi(false));
		model.addAttribute("atendidas",boletaService.countByEstadi(true));
		model.addAttribute("finalizadas", boletaService.countFinalizadas(true, true));
		model.addAttribute("delivery", boletaService.countByTipo("delivery"));
		model.addAttribute("tienda", boletaService.countByTipo("tienda"));
		return "admin/dashboard/index";
	}
	
	@GetMapping("/productos")
	public String listarProductos(Model model) {
		Producto producto = new Producto();
		model.addAttribute("producto", producto);
		model.addAttribute("listaCategorias",categoriaService.listar());
		model.addAttribute("listaProveedores",proveedorService.listar());
		model.addAttribute("listaProductos", productoService.listar());
		return "admin/productos/index";
	}

	@RequestMapping("/productos/form")
	public String guardarProducto(Producto producto, @RequestParam("img") MultipartFile file) throws IOException {

		if (producto.getId() == null) {// cuando se crea un producto
			String nombreImagen = upload.guardarImagen(file);
			producto.setImagen(nombreImagen);
		} else {
			if (file.isEmpty()) {// esitamos el producto pero no se cambia la imagen
				Producto p = new Producto();
				p = productoService.get(producto.getId()).get();
				producto.setImagen(p.getImagen());
			} else {// cuando se edita tambien la imagen

				Producto p = new Producto();
				p = productoService.get(producto.getId()).get();

				if (!p.getImagen().equals("default.jpg")) {
					upload.elimarImagen(p.getImagen());
				}
				String nombreImagen = upload.guardarImagen(file);
				producto.setImagen(nombreImagen);
			}
		}

		productoService.save(producto);
		return "redirect:/admin/productos";
	}

	@RequestMapping("/productos/eliminar/{id}")
	public String eliminarProducto(@PathVariable(value = "id") Long id) {
		Producto p = new Producto();
		p = productoService.get(id).get();

		if (!p.getImagen().equals("default.jpg")) {
			upload.elimarImagen(p.getImagen());
		}

		productoService.delete(id);
		return "redirect:/admin/productos";
	}

	@RequestMapping("/categorias")
	public String listarCategorias(Model model) {
		Categoria categoria = new Categoria();
		model.addAttribute("categoria", categoria);
		model.addAttribute("listaCategorias", categoriaService.listar());
		return "admin/categorias/index";
	}

	@RequestMapping("/categorias/form")
	public String guardar(Categoria categoria) {
		categoriaService.save(categoria);
		return "redirect:/admin/categorias";
	}

	@RequestMapping("/categorias/eliminar/{id}")
	public String eliminar(@PathVariable(value = "id") Long id) {
		categoriaService.delete(id);
		return "redirect:/admin/categorias";
	}

	@RequestMapping("/proveedores")
	public String listarProveedores(Model model) {
		Proveedor proveedor = new Proveedor();
		model.addAttribute("proveedor", proveedor);
		model.addAttribute("listaProveedores", proveedorService.listar());
		return "admin/proveedores/index";
	}

	@RequestMapping("/proveedores/form")
	public String guardarProveedor(Proveedor proveedor) {
		proveedorService.save(proveedor);
		return "redirect:/admin/proveedores";
	}
	
	@RequestMapping("/proveedores/eliminar/{id}")
	public String eliminarProveedor(@PathVariable(value = "id") Long id) {
		proveedorService.delete(id);
		return "redirect:/admin/proveedores";
	}
	
	
	@RequestMapping("/clientes")
	public String listarClientes(Model model) {
		Cliente cliente = new Cliente();
		model.addAttribute("cliente", cliente);
		model.addAttribute("listaClientes", clienteService.listar());
		return "admin/clientes/index";
	}
	
	@RequestMapping("/ordenes")
	public String ordenes(Model model) {
		model.addAttribute("listaBoletas", boletaService.fiandAll());
		return "admin/ordenes/index";
	}
	
	@RequestMapping("/ordenesatendidas")
	public String ordenesate(Model model) {
		model.addAttribute("listaBoletas", boletaService.findByEstado(true,false));
		return "admin/ordenes/atendidas";
	}
	
	@RequestMapping("/ordenesfaltantes")
	public String ordenesfa(Model model) {
		model.addAttribute("listaBoletas", boletaService.findByEstado(false,false));
		return "admin/ordenes/faltantes";
	}

	@RequestMapping("/lista/{id}")
	public String detalles(@PathVariable Long id,  Model model) {
		Optional<Boleta> boleta=boletaService.findById(id);
		model.addAttribute("estado", boleta.get().isEstado());
		model.addAttribute("detalles",boleta.get().getDetalle());
		model.addAttribute("numero",boleta.get().getNumero());
		model.addAttribute("pago", boleta.get().isPago());
		return "admin/ordenes/lista";
	}
	
	@RequestMapping("/atender/{id}")
	public String atender(@PathVariable Long id,Model model) {
		Optional<Boleta> boleta=boletaService.findById(id);
		boletaService.updateEstado(boleta.get().getId());
		return "redirect:/admin/dashboard";
	}
	
	@RequestMapping("/cobrar/{id}")
	public String cobrar(@PathVariable Long id,Model model) {
		Optional<Boleta> boleta=boletaService.findById(id);
		boletaService.updatePago(boleta.get().getId());
		return "redirect:/admin/dashboard";
	}
	
	@RequestMapping("/ordenesdelivery")
	public String delivery(Model model) {
		model.addAttribute("listaBoletas", boletaService.findByTipo("delivery"));
		return "admin/ordenes/atendidas";
	}
	
	@RequestMapping("/ordenestienda")
	public String tienda(Model model) {
		model.addAttribute("listaBoletas", boletaService.findByTipo("tienda"));
		return "admin/ordenes/atendidas";
	}
	
	
	@RequestMapping("/entregar/{id}")
	public String entregar(@PathVariable Long id,Model model) {
		Optional<Boleta> boleta=boletaService.findById(id);
		boletaService.updateEntrega(boleta.get().getId());
		return "redirect:/admin/dashboard";
	
}
	
	@RequestMapping("/ordenesfinalizadas")
	public String fin(Model model) {
		model.addAttribute("listaBoletas", boletaService.findByEstado(true,true));
		return "admin/ordenes/faltantes";
	}
}